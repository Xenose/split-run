#define _GNU_SOURCE
#include <sched.h>
#include<unistd.h>
#include<signal.h>
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include <sys/wait.h>

#define Main main
#define STACK_SIZE 2048

typedef struct {
	char* command;
	char* const* line;
} Command;

Command command;
int status = 0;
pid_t pid;
char stack[STACK_SIZE];

int Run(void* va)
{
	setpgid(getpid(), 0); 
	execvp(command.command, command.line);
	perror("Error :");
	return 0x0;
}

int Main(int arc, char* const* arv)
{
	if (1 == arc)
		goto EXIT;

	command.command = (char*)arv[1];
	command.line = &arv[1];

	pid = clone(&Run, &stack[STACK_SIZE], CLONE_VM, NULL);
	wait(&status);
	kill(getppid(), SIGKILL);
EXIT:
	return 0x0;
}
