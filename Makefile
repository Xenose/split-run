

CC=clang
NAME=srun


default:
	$(CC) *.c -o $(NAME)

install:
	sudo cp $(NAME) /bin/$(NAME)

remove:
	rm /bin/$(NAME)
